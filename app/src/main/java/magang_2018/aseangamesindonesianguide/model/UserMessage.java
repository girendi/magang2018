package magang_2018.aseangamesindonesianguide.model;

public class UserMessage {
    private String user_key;

    public UserMessage() {
    }

    public UserMessage(String user_key) {
        this.user_key = user_key;
    }

    public String getUser_key() {
        return user_key;
    }

    public void setUser_key(String user_key) {
        this.user_key = user_key;
    }
}
